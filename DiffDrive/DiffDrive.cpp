#ifndef DIFF_CPP
#define DIFF_CPP

#include "DiffDrive.h"

DiffDrive::DiffDrive(	byte pwm1, byte dir1, byte dir2, 
							byte pwm2, byte dir3, byte dir4):
							pwm1(pwm1), dir1(dir1), dir2(dir2), 
							pwm2(pwm2), dir3(dir3), dir4(dir4)
{
	pinMode(pwm1, OUTPUT);
	pinMode(dir1, OUTPUT);
	pinMode(dir2, OUTPUT);
	
	pinMode(pwm2, OUTPUT);
	pinMode(dir3, OUTPUT);
	pinMode(dir4, OUTPUT);
	
	analogWrite(pwm1, 0);
	analogWrite(pwm2, 0);
}

void DiffDrive::Forward(byte vel)
{
	analogWrite(pwm1, vel);
	analogWrite(pwm2, vel);
	
	digitalWrite(dir1, HIGH);
	digitalWrite(dir2, LOW);
	digitalWrite(dir3, HIGH);
	digitalWrite(dir4, LOW);
}

void DiffDrive::Backward(byte vel)
{
	analogWrite(pwm1, vel);
	analogWrite(pwm2, vel);
	
	digitalWrite(dir1, LOW);
	digitalWrite(dir2, HIGH);
	digitalWrite(dir3, LOW);
	digitalWrite(dir4, HIGH);
}

void DiffDrive::Brake()
{
	digitalWrite(dir1, HIGH);
	digitalWrite(dir2, HIGH);
	digitalWrite(dir3, HIGH);
	digitalWrite(dir4, HIGH);
	
	analogWrite(pwm1, 0);
	analogWrite(pwm2, 0);
}

void DiffDrive::Left(byte vel)
{
	analogWrite(pwm1, vel);
	analogWrite(pwm2, vel);
	
	digitalWrite(dir1, LOW);
	digitalWrite(dir2, HIGH);
	digitalWrite(dir3, HIGH);
	digitalWrite(dir4, LOW);
}

void DiffDrive::Right(byte vel)
{
	analogWrite(pwm1, vel);
	analogWrite(pwm2, vel);
	
	digitalWrite(dir1, HIGH);
	digitalWrite(dir2, LOW);
	digitalWrite(dir3, LOW);
	digitalWrite(dir4, HIGH);
}

void DiffDrive::Drive(byte vel, int8_t ldir, int8_t rdir)
{
	if(vel > 0)
	{
		if(ldir > 0 && rdir > 0)
			Forward(vel);
		else if(ldir < 0 && rdir < 0)
			Backward(vel);
		else if(ldir < 0 && rdir > 0)
			Left(vel);
		else if(ldir > 0 && rdir < 0)
			Right(vel);
	}
	else
	{
		Brake();
	}
}

#endif
