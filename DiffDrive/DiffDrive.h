#ifndef DIFF_H
#define DIFF_H

#include "Arduino.h"

class DiffDrive
{
	private:
	
		byte pwm1, pwm2, dir1, dir2, dir3, dir4;
	
	public:
		
		DiffDrive(	byte pwm1, byte dir1, byte dir2, 
					byte pwm2, byte dir3, byte dir4);
		
		void Forward(byte vel);
		void Backward(byte vel);
		void Brake();
		void Left(byte vel);
		void Right(byte vel);
		
		void Drive(byte vel, int8_t ldir, int8_t rdir);
};

#endif
